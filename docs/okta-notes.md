# Catatan Okta #

## Resource Owner / Users ##

* Username : wallet001@yopmail.com
* Password : Abcd1234

## Aplikasi Client ##

* Client ID : 0oailac2upht2uIOd356
* Client Secret : PUZJVlac15GRjtjefuQmEkyPBItPsu46ZhuykHO_

## Endpoints ##

* Authorization Endpoint : https://dev-393053.okta.com/oauth2/default/v1/authorize
* Token Endpoint : https://dev-393053.okta.com/oauth2/default/v1/token
* Issuer URI : https://dev-393053.okta.com/oauth2/default

## URLs ##

* Authorization Request : https://dev-393053.okta.com/oauth2/default/v1/authorize?client_id=0oailac2upht2uIOd356&response_type=code&redirect_uri=http://localhost:8080/login/oauth2/code/okta&scope=openid profile&state=test
* OpenID Provider Config : https://dev-393053.okta.com/oauth2/default/.well-known/openid-configuration
* Authorization Server Config : https://dev-393053.okta.com/oauth2/default/.well-known/oauth-authorization-server

## Referensi ##

* https://medium.com/@darutk/diagrams-of-all-the-openid-connect-flows-6968e3990660

